// Package main is the main program for the "delete-random" project.
package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
)

var (
	directory string
	size string
	target string
)


func parseSize(s string) (int64, error) {
	var size int
	var unit string
	_, err := fmt.Sscanf(s, "%d%s", &size, &unit)
	if err != nil {
		return 0, err
	}
	switch unit {
	case "M":
		return int64(size * 1024 * 1024), nil
	case "G":
		return int64(size * 1024 * 1024 * 1024), nil
	default:
		return 0, fmt.Errorf("invalid size specification: %q", s)
	}
}

func totalSize(files []os.FileInfo) int64 {
	var ret int64
	for _, fi := range files {
		ret += fi.Size()
	}
	return ret
}

func remove(file os.FileInfo) error {
	if err := os.Remove(filepath.Join(directory, file.Name())); err != nil {
		return fmt.Errorf("Failed to remove %q: %s", filepath.Join(directory, file.Name()), err)
	}
	fmt.Printf("Deleted %q: %dMB\n", file.Name(), file.Size() / 1024 / 1024)
	return nil
}


func init() {
	flag.StringVar(&directory, "directory", "", "Directory from which to delete files.")
	flag.StringVar(&size, "size", "", "Total file size to be deleted. Expressed in megabytes or gigabytes as 2G or 2M.")
	flag.StringVar(&target, "target", "", "Total file size to remain in directory after deletion.  If set, --size flag is ignored.")
}

func main() {
	 flag.Parse()
	if size == "" && target == "" {
		panic("Either the --size or --target flag must be specified.")
	}
	files, err := ioutil.ReadDir(directory)
	rand.Shuffle(len(files), func(i, j int) { files[i], files[j] = files[j], files[i]})
	if err != nil {
		panic(fmt.Sprintf("Failed to read directory contents: %s", err))
	}
	if target != "" {
		tg_size, err := parseSize(target)
		if err != nil {
			panic(fmt.Sprintf("--target: %s", err))
		}
		for totalSize(files) > tg_size {
			file := files[len(files)-1]
			if err := remove(file); err != nil {
				panic(err)
			}
			files = files[:len(files)-1]
		}
	} else {  // --size was set
		tg_size, err := parseSize(size)
		if err != nil {
			panic(fmt.Sprintf("--size: %s", err))
		}
		var removed int64
		for _, file := range files {
			if err := remove(file); err != nil {
				panic(err)
			}
			removed += file.Size()
			if removed >= tg_size {
				break
			}
		}	
	}


}